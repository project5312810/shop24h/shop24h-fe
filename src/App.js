

import 'bootstrap/dist/css/bootstrap.min.css'
import './App.scss';
import Header from './components/header/Header';
import Footer from './components/footer/Footer';
import Content from './components/content/Content';
import { Route, Routes } from 'react-router';
import Products from './components/products/Product';

function App() {
  return (
    <div>
      <Header />
      <Routes>
        <Route path='/' element={<Content />} />
        <Route path='/products' element={<Products />} />
      </Routes>
      <Footer />
    </div>
  );
}

export default App;
