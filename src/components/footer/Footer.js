import { Col, Container, Nav, NavItem, NavLink, Row } from "reactstrap"
import { FaFacebook, FaTwitter, FaYoutube } from 'react-icons/fa'
import { RiInstagramFill } from 'react-icons/ri'
import Logo from "../Logo"

function Footer() {
    return (
        <div id="footer" className="bg-light py-5">
            <Container>
                <Row>
                    <Col md={3} xs={12} className="mb-4">
                        <h6 className="fw-bold">PRODUCTS</h6>
                        <Nav vertical>
                            <NavItem>
                                <NavLink className="px-0 text-black" href="#">
                                    Help Center
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="px-0 text-black" href="#">
                                    Contact Us
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="px-0 text-black" href="#">
                                    Product Help
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="px-0 text-black" href="#">
                                    Warranty
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="px-0 text-black" href="#">
                                    Order Status
                                </NavLink>
                            </NavItem>
                        </Nav>
                    </Col>
                    <Col md={3} xs={12} className="mb-4">
                        <h6 className="fw-bold">SERVICES</h6>
                        <Nav vertical>
                            <NavItem>
                                <NavLink className="px-0 text-black" href="#">
                                    Help Center
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="px-0 text-black" href="#">
                                    Contact Us
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="px-0 text-black" href="#">
                                    Product Help
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="px-0 text-black" href="#">
                                    Warranty
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="px-0 text-black" href="#">
                                    Order Status
                                </NavLink>
                            </NavItem>
                        </Nav>
                    </Col>
                    <Col md={3} xs={12} className="mb-4">
                        <h6 className="fw-bold">SUPPORT</h6>
                        <Nav vertical>
                            <NavItem>
                                <NavLink className="px-0 text-black" href="#">
                                    Help Center
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="px-0 text-black" href="#">
                                    Contact Us
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="px-0 text-black" href="#">
                                    Product Help
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="px-0 text-black" href="#">
                                    Warranty
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="px-0 text-black" href="#">
                                    Order Status
                                </NavLink>
                            </NavItem>
                        </Nav>
                    </Col>
                    <Col md={3} xs={12} className="mb-4">
                        <Logo />
                        <Nav>
                            <NavItem>
                                <NavLink className="ps-0 text-black" href="#">
                                    <FaFacebook />
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="ps-0 text-black" href="#">
                                    <RiInstagramFill />
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="ps-0 text-black" href="#">
                                    <FaYoutube />
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="ps-0 text-black" href="#">
                                    <FaTwitter />
                                </NavLink>
                            </NavItem>
                        </Nav>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default Footer