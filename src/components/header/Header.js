import { useState } from 'react'
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink
} from 'reactstrap'
import { FaRegBell, FaRegUserCircle } from 'react-icons/fa'
import { FiShoppingCart } from 'react-icons/fi'
import Logo from '../Logo'
import { NavLink as Link } from 'react-router-dom'

function Header() {
    const [isOpen, setIsOpen] = useState(false)

    const toggle = () => setIsOpen(!isOpen)

    return (
        <div>
            <Navbar expand='md' fixed='top' container className='bg-white'>
                <NavbarBrand href="/">
                    <Logo />
                </NavbarBrand>
                <NavbarToggler onClick={toggle} />
                <Collapse isOpen={isOpen} navbar>
                    <Nav navbar style={{ flex: 1 }}>
                        <NavItem >
                            <Link to="/products" className='fw-bold'>Products</Link>
                        </NavItem>
                    </Nav>
                    <Nav navbar>
                        <NavItem>
                            <NavLink href="/"><FaRegBell /></NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="/"><FaRegUserCircle /></NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="/"><FiShoppingCart /></NavLink>
                        </NavItem>
                    </Nav>
                </Collapse>
            </Navbar>
        </div>
    )
}

export default Header