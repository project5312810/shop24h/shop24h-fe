import axios from "axios"
import { useEffect, useState } from "react"
import LastestProducts from "../content/LastestProducts"

function Products() {
    const [products, setProducts] = useState([])

    useEffect(() => {
        const getProducts = async () => {
            const result = await axios.get('http://localhost:8000/api/products')
            setProducts(result.data.data)
        }
        getProducts()
    }, [])
    return (
        <div style={{ marginTop: '71px' }}>
            <LastestProducts data={products} title='ALL PRODUCTS' />
        </div>
    )
}

export default Products