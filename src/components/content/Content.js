import { Container } from "reactstrap"
import Carousel from "./Carousel"
import LastestProducts from "./LastestProducts"
import ViewAll from "./ViewAll"
import axios from "axios"
import { useEffect, useState } from "react"

function Content() {
    const [products, setProducts] = useState([])
    const [limit, setLimit] = useState(8)

    useEffect(() => {
        const getProducts = async () => {
            const result = await axios.get('http://localhost:8000/api/products?limit=' + limit)
            setProducts(result.data.data)
        }
        getProducts()
    }, [limit])

    return (
        <div style={{ marginTop: '71px' }}>
            <Container>
                <Carousel />
                <LastestProducts data={products} title='Lastest Products' />
                <ViewAll click={() => setLimit(0)} />
            </Container>
        </div>
    )
}

export default Content