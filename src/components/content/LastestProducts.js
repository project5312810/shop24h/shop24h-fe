import PropTypes from 'prop-types'
import { Link } from "react-router-dom"
import { Col, Container, Row } from "reactstrap"

function LastestProducts({ data, title }) {
    return (
        <div className="py-3 products">
            <Container>
                <h4 className="text-center pb-4">{title}</h4>
                <Row style={{ rowGap: '40px' }}>
                    {data.length > 0
                        &&
                        data.map(product => (
                            <Col key={product._id} md={3} sm={6} xs={12} className='product'>
                                <div className="view overflow-hidden">
                                    <img src={product.imageUrl} alt="product" />
                                    <button className="add-to-cart">ADD TO CART</button>
                                </div>
                                <Link to='/'>
                                    <h5 className="fw-normal text-center pt-3 pb-2 title">{product.name}</h5>
                                    <div className="price text-center">
                                        {product.buyPrice > product.promotionPrice
                                            &&
                                            <span className='me-2 text-secondary text-decoration-line-through' style={{ fontSize: '14px' }}>${product.buyPrice}</span>
                                        }
                                        <span className="fw-bold text-danger">${product.promotionPrice}</span>
                                    </div>
                                </Link>
                            </Col>
                        ))
                    }
                </Row>
            </Container>
        </div>
    )
}

LastestProducts.propTypes = {
    data: PropTypes.array.isRequired
}

export default LastestProducts