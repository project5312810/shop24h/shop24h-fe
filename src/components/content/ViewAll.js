import PropTypes from 'prop-types'
import { Button } from "reactstrap"

function ViewAll({ click }) {
    return (
        <div className="d-flex justify-content-center py-5">
            <Button color="dark px-4 py-2 rounded-0" onClick={click}>VIEW ALL</Button>
        </div>
    )
}

ViewAll.propTypes = {
    click: PropTypes.func.isRequired
}

export default ViewAll