import { Swiper, SwiperSlide } from "swiper/react"
import { Button, Col, Row } from "reactstrap"
import { Navigation } from "swiper"
import { useRef } from "react"
import { FaChevronLeft, FaChevronRight } from "react-icons/fa"

import freakishClock from '../../assets/images/freakish-clock.png'

import "swiper/css"
import "swiper/css/navigation"

function Carousel() {
    const swiperRef = useRef()

    return (
        <div className="carousel">
            <Swiper
                navigation={true}
                modules={[Navigation]}
                loop={true}
                onBeforeInit={swiper => swiperRef.current = swiper}
            >
                <SwiperSlide>
                    <Row style={{ minHeight: '400px' }}>
                        <Col md={6} className='d-flex align-items-center'>
                            <div>
                                <h5>Decoration</h5>
                                <h1 className="display-4 fw-bold">Freakish Clock</h1>
                                <p className="py-3">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                <Button size="sm" className="bg-black border-0 rounded-0 px-4 py-2">SHOP NOW</Button>
                            </div>
                        </Col>
                        <Col md={6} className='d-flex align-items-center justify-content-center'>
                            <img src={freakishClock} style={{ width: '60%' }} alt='slide'></img>
                        </Col>
                    </Row>
                </SwiperSlide>
                <SwiperSlide>
                    <Row style={{ minHeight: '400px' }}>
                        <Col md={6} className='d-flex align-items-center'>
                            <div>
                                <h5>Decoratdsadaion</h5>
                                <h1 className="display-4 fw-bold">Freakish Clock</h1>
                                <p className="py-3">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                <Button size="sm" className="bg-black border-0 rounded-0 px-4 py-2">SHOP NOW</Button>
                            </div>
                        </Col>
                        <Col md={6} className='d-flex align-items-center justify-content-center'>
                            <img src={freakishClock} style={{ width: '60%' }} alt='slide'></img>
                        </Col>
                    </Row>
                </SwiperSlide>
            </Swiper>

            <div className="carousel-prev-btn" onClick={() => swiperRef.current?.slidePrev()}><FaChevronLeft /></div>
            <div className="carousel-next-btn" onClick={() => swiperRef.current?.slideNext()}><FaChevronRight /></div>
        </div>
    )
}

export default Carousel